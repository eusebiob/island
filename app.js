let transform
(transform = () => {
  //Movement Animation to happen
  const fence = document.querySelector(".fence");
  const container = document.querySelector(".fence-container");
  const drag = document.querySelector(".fence-drag");
  const rotate = document.querySelector(".fence-rotate");
  const skew = document.querySelector(".fence-skew");
  const main = document.querySelector(".container");
  const resizers = document.querySelectorAll(".fence-resize");
  const flipers = document.querySelectorAll(".fence-flip");


  /* RESIZE CONTAINER */
  resizers.forEach(resizer => {
    resizer.addEventListener('mousedown', mouseDownHandler);
  })

  let xAxis = 0
  let yAxis = 0
  let xySkew = 0
  let xyRotate = 0
  let horizontal = false

  flipers.forEach(fliper => {
    fliper.addEventListener('mousedown', handleFilpMouseDown)
    document.addEventListener('mouseup', () => {
      main.removeEventListener('mousemove', handleContainerChange)
      // main.mousemove = null;
      // document.onmouseup = null;
      // document.onmousemove = null;
      container.style.transition = "none";
    })
  })

  skew.addEventListener('mousedown', handleSkewMouseDown)
  document.addEventListener('mouseup', handleSkewMouseUp)

  function handleSkewMouseDown(element) {
    main.addEventListener("mousemove", handleContainerMouseMove)
  }

  function handleSkewMouseUp(element) {
    main.removeEventListener("mousemove", handleContainerMouseMove)
  }

  function handleContainerMouseMove(event) {
    const coord = skew.getBoundingClientRect()
    const max = 50
    const min = -50
    xySkew = event.clientX - coord.x

    if (xySkew < min)
      xySkew = min

    if (xySkew > max)
      xySkew = max

    container.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg) rotate(${xyRotate}deg) skew(${xySkew}deg)`;
    container.style.transition = "all 0.5s ease";
  }


  // rotate.addEventListener('mousedown', handleRotateMouseDown)

  // function handleRotateMouseDown(event) {
  //   const coord = fence.getBoundingClientRect()
  //   const radius  = fence.offsetWidth / 2;
  //   const center_x  = coord.x + radius;
  //   const center_y  = coord.y + radius;


  //   const click_degrees = get_degrees(event.pageX, event.pageY)
  //   function get_degrees(mouse_x, mouse_y) {

  //     const radians  = Math.atan2(mouse_x - center_x, mouse_y - center_y);
  //     const degrees  = Math.round((radians * (180 / Math.PI) * -1) + 100);

  //     return degrees;
  //   }

  //   fence.addEventListener("mousemove", handleContainerRotate)
  //   document.addEventListener('mouseup', () => {
  //     fence.removeEventListener("mousemove", handleContainerRotate)
  //   })

  //   function handleContainerRotate(event) {
  //     xyRotate = get_degrees(event.pageX, event.pageY) - click_degrees
  //     container.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg) rotate(${xyRotate}deg) skew(${xySkew}deg)`;
  //     container.style.transition = "all 0.5s ease";
  //   }
  // }

  function handleFilpMouseDown(element) {
    horizontal = element.target.classList.contains('fence-flip-left')
    main.addEventListener("mousemove", handleContainerChange);
  }

  function handleContainerChange(event) {
    if (horizontal)
      xAxis = (window.innerWidth / 2 - event.pageX) / 2

    if (!horizontal)
      yAxis = (window.innerHeight / 2 - event.pageY) / 2

    container.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg) rotate(${xyRotate}deg) skew(${xySkew}deg)`;
    container.style.transition = "all 0.5s ease";
  }

  container.addEventListener('mouseleave', (event) => {
    // document.onmouseup = null;
    // document.onmousemove = null;
    // container.onmousedown = null
  })

  drag.addEventListener('mousedown', (event) => {
    dragElement(container)
  })

  drag.addEventListener('mouseup', (event) => {
    document.onmouseup = null;
    document.onmousemove = null;
    container.onmousedown = null
  })

  function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

    elmnt.onmousedown = dragMouseDown;

    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();

      pos3 = e.clientX;
      pos4 = e.clientY;

      document.onmouseup = closeDragElement;
      document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();

      const { dhtop, dhbot, dwtop, dwbot} = calculateDragLimit()

      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;

      let top = (elmnt.offsetTop - pos2)
      let left = (elmnt.offsetLeft - pos1)

      // if (top <= dhtop)
      //   top = dhtop

      // if (top >= dhbot)
      //   top = dhbot

      // if (left <= dwtop)
      //   left = dwtop

      // if (left >= dwbot)
      //   left = dwbot

      elmnt.style.top = top + "px";
      elmnt.style.left = left + "px";
    }

    function closeDragElement() {
      document.removeEventListener('mousemove', elementDrag)
      document.removeEventListener('mouseup', closeDragElement)
      document.onmouseup = null;
      document.onmousemove = null;
    }
  }

  function mouseDownHandler(elmnt) {
    const x = elmnt.clientX
    const y = elmnt.clientY

    // Calculate the dimension of element
    const styles = window.getComputedStyle(fence)
    const w = parseInt(styles.width, 10)
    const h = parseInt(styles.height, 10)
    const horizontal = elmnt.target.classList.contains('fence-resize-right')

    // Attach the listeners to `document`
    document.addEventListener('mousemove', mouseMoveHandler)
    document.addEventListener('mouseup', mouseUpHandler)

    function mouseMoveHandler(elmnt) {
      const dx = elmnt.clientX - x
      const dy = elmnt.clientY - y

      if (horizontal)
        fence.style.width = `${w + dx}px`

      if (!horizontal)
        fence.style.height = `${h + dy}px`
    }

    function mouseUpHandler() {
      document.removeEventListener('mousemove', mouseMoveHandler)
      document.removeEventListener('mouseup', mouseUpHandler)
    }
  }

  function calculateDragLimit() {
    /* WIDTH */
    const fenceWidth = fence.clientWidth // 1500
    const dragWidth = drag.clientWidth // 148
    const mainWidth = main.clientWidth // 2500

    const dwCard = (fenceWidth / 2)
    const dwDrag = (dragWidth / 2)

    const dwtop = -Math.abs(-(dwCard - dwDrag))
    const dwbot = ((mainWidth / dwCard) * dwCard) - (dwDrag + dwCard)

    /* HEIGHT */
    const fenceHeight = fence.clientHeight // 1500
    const dragHeight = drag.clientHeight // 148
    const mainHeight = main.clientHeight // 2500

    const dhCard = (fenceHeight / 2)
    const dhDrag = (dragHeight / 2)

    const dhtop = -Math.abs(-(dhCard - dhDrag))
    const dhbot = ((mainHeight / dhCard) * dhCard) - (dhDrag + dhCard)

    return { dhtop, dhbot, dwtop, dwbot }
  }
})()

;(() => {
  const color = document.querySelector('input[type="color"]')

  const save = document.querySelector(".background-save")
  const container = document.querySelector(".container")
  const button = document.querySelector(".background-button")
  const uploadgateButton = document.querySelector(".uploadgate-button")
  const uploadfile = document.querySelector('input[name="background"]')
  const uploadgate = document.querySelector('input[name="uploadgate"]')

  const image = document.querySelector('.background img')
  const fenceImage = document.querySelector(".fence img")

  /*Convert image to base64 */

  button.addEventListener('click', () => {
    uploadfile.click()
  })

  uploadfile.addEventListener('change', function() {
    const file = this.files[0]
    if (file) {
      const reader = new FileReader();
        reader.onloadend = () => {
        image.src = reader.result
      }
      reader.readAsDataURL(file)
    }
  })

  uploadgateButton.addEventListener('click', () => {
    uploadgate.click()
  })

uploadgate.addEventListener('change', function() {
  const file = this.files[0]
  if (file) {
    const reader = new FileReader();
      reader.onloadend = () => {
      fenceImage.src = reader.result
    }
    reader.readAsDataURL(file)
  }
})


save.addEventListener('click', () => {
  const pdf = new jsPDF()
  pdf.addHTML($(".container"), function() {
    const blob = pdf.output("blob")
    window.open(URL.createObjectURL(blob))
  })
})

color.addEventListener('change', (event) => {
  updateSVGColor(event.target.value)
})


// preview canvas
/*
  save.addEventListener('click', () => {
    // image.onload = () => {
    //   URL.revokeObjectURL(image.src)
    // }
    // image.src = URL.createObjectURL(this.files[0])
    html2canvas($(`.container`), {
      onrendered: function(canvas) {
        // const context = canvas.getContext('webgl') || canvas.getContext("experimental-webgl")
        const context = canvas.getContext("2d")
        const img = new Image()
        const fimg = new Image()

        const offsets = fenceImage.getBoundingClientRect()
        const offsetsContainer = container.getBoundingClientRect()

        img.src = image.src
        img.onload = function() {

          const imgRatio = img.height / img.width
          const containerRatio = offsetsContainer.height / offsetsContainer.width

          if (imgRatio > containerRatio) {
            const height = containerRatio * imgRatio
            context.drawImage(img, 0, (offsetsContainer.height - height) / 2, offsetsContainer.width, height)
          }

          if (imgRatio < containerRatio) {
            const width = offsetsContainer.width * containerRatio / imgRatio
            context.drawImage(img, (offsetsContainer.width - width) / 2, 0, width, offsetsContainer.height)
          }

          fimg.src = fenceImage.src
          fimg.onload = function() {
            imageLeft = parseInt(offsets.left) - parseInt(offsetsContainer.left)
            imageTop = parseInt(offsets.top) - parseInt(offsetsContainer.top)
            context.drawImage(fimg, imageLeft, imageTop)
          }
        }

        canvas.style.marginBottom = '200px'

        document.body.appendChild(canvas)
      }
    })
  })
*/

})()
